package main

import (
	"log"
	"fmt"
	"os"
	"bufio"
	"strings"
	"strconv"
	"os/user"
	"io/ioutil"

	"net/http"
	"encoding/json"

	"github.com/julienschmidt/httprouter"
)


type kill_req struct {
	Pid uint64 `json:"PID"`
}
type proceso struct {
	Total uint64 `json:"total"`
	Ejecucion uint64 `json:"totalejecucion"`
	Suspendidos uint64 `json:"totalsuspendidos"`
	Detenidos uint64 `json:"totaldetenidos"`
	Zombies uint64 `json:"totalzombie"`
	Pid string `json:"PID"`
	Nombre string `json:"NOMBRE"`
	Usuario string `json:"USUARIO"`
	Estado string `json:"ESTADO"`
	Ram string `json:"RAM"`
	pid uint64
	parent uint64
}
type arbol struct {
	Pid string `json:"PID"`
	Nombre string `json:"NOMBRE"`
	Usuario string `json:"USUARIO"`
	Estado string `json:"ESTADO"`
	Ram string `json:"RAM"`
	Hijos []arbol `json:"HIJOS"`
	pid uint64
	parent uint64
}

type wrapper struct {
	Root arbol `json:"root"`
}

func get_username(uid string) string {
	U, err := user.LookupId(uid)
	if err == nil {
		return U.Username
	} else {
		return "Unkown"
	}
}

func build_tree(node arbol, procs []proceso) arbol {
	var pending_procs []proceso
	for i:=0;i<len(procs);i++{
		proc := procs[i]
		if proc.parent == node.pid {
			var hijo arbol
			hijo.Pid = proc.Pid
			hijo.Nombre = proc.Nombre
			hijo.Usuario = proc.Usuario
			hijo.Estado = proc.Estado
			hijo.Ram = proc.Ram
			hijo.pid = proc.pid
			hijo.parent = proc.parent
			node.Hijos = append(node.Hijos, hijo)
		}else {
			pending_procs = append(pending_procs,proc)
		}
	}

	for i:=0;i<len(node.Hijos);i++{
		node.Hijos[i] = build_tree(node.Hijos[i],pending_procs)
	}
	return node
}

func read_module(modulo string) string { /* Lee el modulo entero */
	proc, err := os.Open("/proc/"+modulo)
	if err != nil {
		log.Fatal(err)
	}
	defer func(){
		if err = proc.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	scanner := bufio.NewScanner(proc)
	content := ""
	for scanner.Scan(){
		content = content + scanner.Text()
	}
	return strings.TrimSpace(content)
}

func get_procs() []proceso {
	raw_procs, err := os.Open("/proc/process-tree")
	if err != nil {
		log.Fatal(err)
	}
	defer func(){
		if err = raw_procs.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	var procesos []proceso
	scanner := bufio.NewScanner(raw_procs)
	var total uint64 = 0
	var ejecucion uint64 = 0
	var suspendidos uint64 = 0
	var detenidos uint64 = 0
	var zombies uint64 = 0
	for scanner.Scan(){
		proc := scanner.Text()
		data := strings.Split(proc,"|")
		var ren proceso
		for i:=0; i<len(data);i++ {
			switch i {
				case 0: ren.Nombre = data[i]
				case 1:
					ren.Estado = data[i]
					switch ren.Estado {
						case "running": ejecucion++
						case "parked","sleeping","disk sleep": suspendidos++
						case "stopped","tracing stop","dead": detenidos++
						case "zombie": zombies++
					}
				case 2: ren.Ram = data[i]
				case 3:
					ren.Pid = data[i]
					ren.pid, err = strconv.ParseUint(ren.Pid,10,64)
				case 4: ren.Usuario = get_username(data[i])
				case 5: ren.parent, err = strconv.ParseUint(data[i],10,64)
			}
		}
		procesos = append(procesos,ren)
		total++
	}
	var c uint64
	for c = 0;c<total;c++{
		procesos[c].Total = total;
		procesos[c].Ejecucion = ejecucion;
		procesos[c].Suspendidos = suspendidos;
		procesos[c].Detenidos = detenidos;
		procesos[c].Zombies = zombies;
	}
	return procesos
}

func getRam(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w,read_module("ram-monitor"))
}

func getProcesses(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(get_procs())
}

func getTree(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	var root arbol
	root.pid = 0
	root.parent = 0
	root.Pid = "0"
	root.Nombre = "root"
	root.Usuario = "root"
	root.Estado = "running"
	root.Ram = "0"
	json.NewEncoder(w).Encode(wrapper { Root: build_tree(root,get_procs())})
}

func kill_process(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	fmt.Println("Recieved KILL request.")
	var req kill_req
	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatalf("%s: %s","Failed to read req body",err)
	}

	json.Unmarshal(reqBody, &req) //I should check the err returned by json.Unmarshall
	fmt.Printf("PID: %d",req.Pid)
	p,err := os.FindProcess(int(req.Pid))
	if err != nil {
		log.Fatalf("Process does not exist: %d",req.Pid)
	}
	err = p.Kill()
	if err!= nil {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w,"{\"msg\":\"Failed to kill process\"}")
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w,"{\"msg\":\"OK\"}")
}

func main(){
	router := httprouter.New()
	router.GlobalOPTIONS = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	if r.Header.Get("Access-Control-Request-Method") != "" {
		// Set CORS headers
		header := w.Header()
		header.Set("Access-Control-Allow-Methods", header.Get("Allow"))
		header.Set("Access-Control-Allow-Headers", "*")
		header.Set("Access-Control-Allow-Origin", "*")
	    }
	    w.WriteHeader(http.StatusNoContent)
	})

	router.GET("/getInfoRam", getRam)
	router.GET("/getInfoProcess", getProcesses)
	router.GET("/getTree", getTree)
	router.POST("/kill", kill_process)
	log.Fatal(http.ListenAndServe(":1776", router))
}
